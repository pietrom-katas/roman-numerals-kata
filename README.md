# pietrom-roman-numerals [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Roman Numerals Kata implementation

## Installation

```sh
$ npm install --save pietrom-roman-numerals
```

## Usage

```js
const pietromRomanNumerals = require('pietrom-roman-numerals');

pietromRomanNumerals('Rainbow');
```
## License

MIT © [Pietro Martinelli]()


[npm-image]: https://badge.fury.io/js/pietrom-roman-numerals.svg
[npm-url]: https://npmjs.org/package/pietrom-roman-numerals
[travis-image]: https://travis-ci.org/pietrom/pietrom-roman-numerals.svg?branch=master
[travis-url]: https://travis-ci.org/pietrom/pietrom-roman-numerals
[daviddm-image]: https://david-dm.org/pietrom/pietrom-roman-numerals.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/pietrom/pietrom-roman-numerals
