const sut = require('../index.js')

describe('Decimal to roman', () => {
	describe('Single char numerals', () => {
		it('1 --> I', () => {  	
			expect(sut.decimal2roman(1)).toBe('I')
		})

		it('5 --> V', () => {  	
			expect(sut.decimal2roman(5)).toBe('V')
		})

		it('10 --> X', () => {  	
			expect(sut.decimal2roman(10)).toBe('X')
		})

		it('50 --> L', () => {  	
			expect(sut.decimal2roman(50)).toBe('L')
		})

		it('100 --> C', () => {  	
			expect(sut.decimal2roman(100)).toBe('C')
		})

		it('500 --> D', () => {  	
			expect(sut.decimal2roman(500)).toBe('D')
		})

		it('1000 --> M', () => {  	
			expect(sut.decimal2roman(1000)).toBe('M')
		})
	})

	describe('Different chars, in descending order', () => {
		it('1666 --> MDCLXVI', () => {  	
			expect(sut.decimal2roman(1666)).toBe('MDCLXVI')
		})

		it('1878 --> MDCCCLXXVIII', () => {  	
			expect(sut.decimal2roman(1878)).toBe('MDCCCLXXVIII')
		})	
		
		it('2011 --> MMXI', () => {  	
			expect(sut.decimal2roman(2011)).toBe('MMXI')
		})

		it('2013 --> MMXIII', () => {  	
			expect(sut.decimal2roman(2013)).toBe('MMXIII')
		})
	})

	describe('Different chars, in ascending order too', () => {
		it('1949 --> MCMXLIX', () => {
			expect(sut.decimal2roman(1949)).toBe('MCMXLIX')
		})	

		it('1494 --> MCDXCIV', () => {
			expect(sut.decimal2roman(1494)).toBe('MCDXCIV')
		})	

		it('9 --> IX', () => {  	
			expect(sut.decimal2roman(9)).toBe('IX')
		})
	})
})
