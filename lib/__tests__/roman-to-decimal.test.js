const sut = require('../index.js')

describe('Roman to decimal', () => {
	describe('Single char numerals', () => {
		it('I --> 1', () => {  	
			expect(sut.roman2decimal('I')).toBe(1)
		})

		it('V --> 5', () => {  	
			expect(sut.roman2decimal('V')).toBe(5)
		})

		it('L --> 50', () => {  	
			expect(sut.roman2decimal('L')).toBe(50)
		})

		it('C --> 100', () => {  	
			expect(sut.roman2decimal('C')).toBe(100)
		})

		it('D --> 500', () => {  	
			expect(sut.roman2decimal('D')).toBe(500)
		})

		it('M --> 1000', () => {  	
			expect(sut.roman2decimal('M')).toBe(1000)
		})
	})

	describe('Multiple Is', () => {
		it('II --> 2', () => {  	
			expect(sut.roman2decimal('II')).toBe(2)
		})

		it('III --> 3', () => {  	
			expect(sut.roman2decimal('III')).toBe(3)
		})
	})

	describe('Multiple Ms', () => {
		it('MMM --> 3000', () => {  	
			expect(sut.roman2decimal('MMM')).toBe(3000)
		})

		it('MMMMMM --> 6000', () => {  	
			expect(sut.roman2decimal('MMMMMM')).toBe(6000)
		})
	})

	describe('Different chars, in descending order', () => {
		it('MDCCCLXXVIII --> 1878', () => {  	
			expect(sut.roman2decimal('MDCCCLXXVIII')).toBe(1878)
		})	

		it('MMXI --> 2011', () => {  	
			expect(sut.roman2decimal('MMXI')).toBe(2011)
		})

		it('MMXIII --> 2013', () => {  	
			expect(sut.roman2decimal('MMXIII')).toBe(2013)
		})
	})

	describe('Char couples with ascending value', () => {
		it('IV --> 4', () => {
			expect(sut.roman2decimal('IV')).toBe(4)
		})

		it('IX --> 9', () => {
			expect(sut.roman2decimal('IX')).toBe(9)
		})

		it('XL --> 40', () => {
			expect(sut.roman2decimal('XL')).toBe(40)
		})

		it('CD --> 400', () => {
			expect(sut.roman2decimal('CD')).toBe(400)
		})

		it('CM --> 900', () => {
			expect(sut.roman2decimal('CM')).toBe(900)
		})
	})

	describe('More complex Roman Numerals, containing chars in both ascending and descending order', () => {
		it('MCMLXXVIII --> 1978', () => {
			expect(sut.roman2decimal('MCMLXXVIII')).toBe(1978)
		})

		it('MCMXLIX --> 1949', () => {
			expect(sut.roman2decimal('MCMXLIX')).toBe(1949)
		})

		it('CDLXXVI --> 476', () => {
			expect(sut.roman2decimal('CDLXXVI')).toBe(476)
		})		
	})
})
