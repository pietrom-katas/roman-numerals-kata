'use strict';

const romanValues = {
	I: 1, V:5, X: 10, L: 50, C: 100, D:500, M:1000
}

function isLast(arr, i) {
	return i === arr.length - 1
}

function areDescendingOrEqual(x, y) {
	return romanValues[x] >= romanValues[y]
}

function process(acc, curr, i, arr) {
	const sign = isLast(arr, i) || areDescendingOrEqual(curr, arr[i + 1]) ? 1 : -1
	return acc + sign * romanValues[curr]
}

function roman2decimal(roman) {
	return roman.split('').reduce(process, 0)
}

const decimalValues = {
	1: 'I',
	5: 'V',
	10: 'X',
	50: 'L',
	100: 'C',
	500: 'D',
	1000: 'M'
}

const decimalItems = Object.keys(decimalValues).sort((x, y) => y - x).map(x => parseInt(x))

function times(c, n) {
	let res = ''
	for(let i = 0; i < n; i++) {
		res += c
	}
	return res
}

const cleanups = {
	'VIIII' : 'IX',
	'IIII' : 'IV',
	'DCCCC': 'CM',
	'CCCC': 'CD',
	'LXXXX': 'XC',
	'XXXX': 'XL'
}

function cleanup(roman) {
	return Object.keys(cleanups).reduce((acc, curr) => acc.replace(curr, cleanups[curr]), roman)
}

function decimal2roman(n) {
	return cleanup(decimalItems.reduce((acc, i) => {
		const num = Math.floor(acc.decimal / i)
		return {
			roman: acc.roman + times(decimalValues[i], num),
			decimal: acc.decimal - num * i
		}
	}, {roman: '', decimal: n}).roman)
}

module.exports = {
	roman2decimal, decimal2roman
}
